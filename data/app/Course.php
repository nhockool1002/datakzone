<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table= "course";

    public function user(){
        return $this->belongsTo('App/User','user_id','id');
    }
}
