<!DOCTYPE html>
<html lang="en">
  <head>
    @include('templates.header')
  </head>
  <body>  
    <!-- Left column -->
    <div class="templatemo-flex-row">
      <div class="templatemo-sidebar">
        @include('templates.cover')    
        <!-- Search box -->
        @include('templates.leftside')
      <!-- Main content --> 
      @yield('content')
    </div>
    
    <!-- JS -->
    @include('templates.script')
  </body>
</html>