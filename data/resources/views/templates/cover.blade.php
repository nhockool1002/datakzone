<header class="templatemo-site-header">
    <div class="square"></div>
    <h1>DATA K-ZONE</h1>
  </header>
  <div class="profile-photo-container">
    <img src="{{ asset('lib/images/profile-photo.jpg') }}" alt="Profile Photo" class="img-responsive">  
    <div class="profile-photo-overlay"></div>
  </div>  