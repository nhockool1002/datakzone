<form class="templatemo-search-form" role="search">
    <div class="input-group">
        <button type="submit" class="fa fa-search"></button>
        <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">           
    </div>
  </form>
  <div class="mobile-menu-icon">
      <i class="fa fa-bars"></i>
  </div>
  <nav class="templatemo-left-nav">          
    <ul>
      <li><a href="#" class="active"><i class="fa fa-home fa-fw"></i>Tài liệu - Khóa học</a></li>
    </ul>  
  </nav>
</div>