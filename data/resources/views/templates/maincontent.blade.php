<div class="templatemo-content col-1 light-gray-bg">
@include('templates.userbar')
    <div class="templatemo-content-container">
      <div class="templatemo-flex-row flex-content-row">
        <div class="templatemo-content-widget white-bg col-2">
          <div class="square"></div>
          <h2 class="templatemo-inline-block">Danh sách khóa học</h2><hr>
          <table id="example" class="mdl-data-table" style="width:100%">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Tên khóa học</th>
                    <th>Lượt xem</th>
                    <th>Chi tiết</th>
                </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>[QHOnline] Lập trình ứng dụng với NodeJS</td>
                <td><i class="fa fa-eye"></i> 12</td>
                <td><button class="btn btn-info btn-xs">Xem</button></td>
              </tr>
              <tr>
                <td>2</td>
                <td>[QHOnline] Laravel 5.4 In Action</td>
                <td><i class="fa fa-eye"></i> 11</td>
                <td><button class="btn btn-info btn-xs">Xem</button></td>
              </tr>
              <tr>
                <td>3</td>
                <td>[QHOnline] Lập trình ứng dụng với Python</td>
                <td><i class="fa fa-eye"></i> 30</td>
                <td><button class="btn btn-info btn-xs">Xem</button></td>
              </tr>
              <tr>
                <td>4</td>
                <td>[QHOnline] Lập trình Android</td>
                <td><i class="fa fa-eye"></i> 8</td>
                <td><button class="btn btn-info btn-xs">Xem</button></td>
              </tr>
              <tr>
                <td>5</td>
                <td>[QHOnline] Lập trình iOS với Swift 2</td>
                <td><i class="fa fa-eye"></i> 7</td>
                <td><button class="btn btn-info btn-xs">Xem</button></td>
              </tr>
              <tr>
                <td>6</td>
                <td>[QHOnline] Lập trình CodeIgniter Framework</td>
                <td><i class="fa fa-eye"></i> 3</td>
                <td><button class="btn btn-info btn-xs">Xem</button></td>
              </tr>
              <tr>
                <td>7</td>
                <td>[QHOnline] Lập trình Zend Framework 2.X</td>
                <td><i class="fa fa-eye"></i> 12</td>
                <td><button class="btn btn-info btn-xs">Xem</button></td>
              </tr>
            </tbody>
            <tfoot>
                <tr>
                  <th>STT</th>
                  <th>Tên khóa học</th>
                  <th>Lượt xem</th>
                  <th>Chi tiết</th>
                </tr>
            </tfoot>
        </table>             
        </div>
      </div>
      @include('templates.footer')     
    </div>
  </div>